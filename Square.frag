#ifdef GL_ES
precision mediump float;
#endif

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 st = gl_FragCoord.xy/iResolution.xy;
    vec2 bl = step(vec2(0.1),st);
    vec2 tr = step(vec2(0.1),1.0-st);
    vec3 color = vec3(bl.x * bl.y * tr.x * tr.y);
    fragColor = vec4(color,1.0);
}