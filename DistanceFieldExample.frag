#ifdef GL_ES
precision mediump float;
#endif

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 st = fragCoord.xy/iResolution.xy;
    st.x *= iResolution.x/iResolution.y;
    st = st * 2.0 - 1.0;
    float d = length( abs(st)- sin(iTime) * .3);

    fragColor = vec4(vec3(fract(d*10.0)),1.0);
}