#ifdef GL_ES
precision mediump float;
#endif

vec3 colorBlue = vec3(0.149,0.141,0.912);
vec3 colorYellow = vec3(1.000,0.833,0.224);
float PI = 3.14;

vec4 circle(vec2 uv, vec2 pos, float rad, vec3 color) {
    float l = length(pos-uv);
	float d = l - rad;
	float t = clamp(d, 0.0, 1.0);

    if(t < 1.0){
        //t = l/rad;
        t = smoothstep(0.5,1.0,l/rad);
    }

	return vec4(color, 1.0 - t);
}

float functionSun(float x){
    return 1.0 - pow(abs(sin(PI*x / 3.0)),0.5);
}

float functionHelper(float x){
    return (x*2.0) - 1.0;
}

void mainImage(out vec4 fragColor, in vec2 fragCoord){
    //SUN
    vec2 uv = fragCoord.xy;
    vec2 circlePos = vec2(iResolution.x * 0.8,iResolution.y * 0.4);
    float radius = 0.1 * iResolution.y;
    vec4 sun = circle(uv,circlePos,radius,vec3(1.0,0.67,0.0));

    //Background
    vec2 scaledUV = fragCoord.xy/iResolution.xy;
    float toMinusOne = functionHelper(scaledUV.y);
    vec3 color = mix(colorYellow,colorBlue,functionSun(toMinusOne));    
    vec4 background = vec4(color,1.0);
    
    //fragColor = vec4(color,1.0);
    fragColor = mix(background,sun,sun.a);
}