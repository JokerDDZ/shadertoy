#ifdef GL_ES
precision mediump float;
#endif

vec3 colorYellow = vec3(1.0,1.0,0.0);
vec3 colorBlue = vec3(0.0,0.0,1.0);

void mainImage(out vec4 fragColor,in vec2 fragCoord){
    vec2 uv = fragCoord.xy/iResolution.xy;
    vec3 color = mix(colorBlue,colorYellow,pow(uv.x,2.0));
    fragColor = vec4(color,1.0);
}